#
# GNU Makefile
#

PROGRAM=solve

CXX=g++
CXXFLAGS=-O2 -g0 -Wall -Wno-unused-variable

all: $(PROGRAM)

$(PROGRAM): solve.o
	$(CXX) -Wl,--warn-common $^ -o $@ -lm

clean:
	rm -f $(PROGRAM) *.o ERRS

.PHONY: all clean

#--- editor settings ------------
# vi:ts=8:sw=8:noexpandtab
