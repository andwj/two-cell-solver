//
//  Two-Cell Solver
//
//  by Andrew Apted, 2017.
//
//  License of this code is CC0 (public domain).
//  USE AT OWN RISK.
//


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <ctype.h>
#include <math.h>
#include <errno.h>

#include <map>
#include <vector>
#include <algorithm>


#define MSG_BUF_LEN  2000

void FatalError(const char *msg, ...)
{
	static char buffer[MSG_BUF_LEN];

	va_list arg_pt;

	va_start(arg_pt, msg);
	vsnprintf(buffer, MSG_BUF_LEN-1, msg, arg_pt);
	va_end(arg_pt);

	buffer[MSG_BUF_LEN-2] = 0;

	fprintf(stderr, "ERROR! %s\n", buffer);

	exit(9);
}


// these are chosen so that black are odd, red are even
typedef enum
{
	SUIT_Clubs    = 0,
	SUIT_Diamonds = 1,
	SUIT_Spades   = 2,
	SUIT_Hearts   = 3

} Suit_e;


// this is rank (1..13) * 4 + suit (0..3)
// Note that zero is NOT a valid card number.
typedef unsigned char Card_t;


static Card_t card_create(int rank, Suit_e suit)
{
	return (Card_t)(rank * 4 + (int)suit);
}

static inline int card_rank(const Card_t card)
{
	return (int)(card >> 2);
}

static inline Suit_e card_suit(const Card_t card)
{
	return (Suit_e)(card & 3);
}

static inline bool can_stack_card(const Card_t card, const Card_t onto)
{
	if (card_rank(card) + 1 != card_rank(onto))
		return false;

	// suits must be different colors
	return ((int)card & 1) != ((int)onto & 1);
}

static const char * card_to_string(const Card_t card);



#define MAX_FREE    2
#define MAX_COLUMN  64


#define COLUMN_END  0xff

struct Packed_state_t
{
	unsigned char pile1, pile2;

	unsigned char free[MAX_FREE];

	unsigned char board[61];	// each column ends with 'COLUMN_END', and
								// after the last column are zeros.

	bool operator< (const Packed_state_t& rhs) const
	{
		if (pile1 != rhs.pile1)
			return pile1 < rhs.pile1;

		if (pile2 != rhs.pile2)
			return pile2 < rhs.pile2;

		int cmp = memcmp(&free, &rhs.free, sizeof(free));

		if (cmp != 0)
			return cmp < 0;

		cmp = memcmp(&board, &rhs.board, sizeof(board));

		return cmp < 0;
	}
};


class Game_state_c
{
public:
	int pile[4];	// indexed by SUIT_XXX

	Card_t free[MAX_FREE];	// zero for unused ones

	int num_free;

	Card_t column[8][MAX_COLUMN];

	int num_rows[8];

private:
	void Clear()
	{
		for (int p = 0 ; p < 4 ; p++)
			pile[p] = 0;

		num_free = 0;

		for (int f = 0 ; f < MAX_FREE ; f++)
			free[f] = 0;

		memset(&column, 0, sizeof(column));

		for (int c = 0 ; c < 8 ; c++)
			num_rows[c] = 0;
	}

public:
	Game_state_c()
	{
		Clear();
	}

	Game_state_c(const Game_state_c& src)
	{
		memset(&column, 0, sizeof(column));

		for (int p = 0 ; p < 4 ; p++)
			pile[p] = src.pile[p];

		num_free = src.num_free;

		for (int f = 0 ; f < num_free ; f++)
			free[f] = src.free[f];

		for (int col = 0 ; col < 8 ; col++)
		{
			num_rows[col] = src.num_rows[col];

			for (int i = 0 ; i < num_rows[col] ; i++)
			{
				column[col][i] = src.column[col][i];
			}
		}
	}

	Game_state_c(const Packed_state_t& src)
	{
		Clear();

		pile[0] = (src.pile1 & 15);
		pile[1] = (src.pile1 >> 4);

		pile[2] = (src.pile2 & 15);
		pile[3] = (src.pile2 >> 4);

		for (int f = 0 ; f < MAX_FREE ; f++)
		{
			free[f] = (Card_t) src.free[f];

			if (free[f] > 0)
				num_free = f + 1;
		}

		int col = 0;
		int row = 0;

		for (int ofs = 0 ; ofs < 64 ; ofs++)
		{
			// all done?
			if (src.board[ofs] == 0)
				break;

			if (src.board[ofs] == COLUMN_END)
			{
				num_rows[col] = row;

				col += 1;
				row = 0;
				continue;
			}

			column[col][row] = (Card_t) src.board[ofs];

			row += 1;
		}
	}

	~Game_state_c()
	{ }

	void pack_it(Packed_state_t& dest)
	{
		dest.pile1 = (pile[1] << 4) + pile[0];
		dest.pile2 = (pile[3] << 4) + pile[2];

		for (int f = 0 ; f < MAX_FREE ; f++)
		{
			if (f < num_free)
				dest.free[f] = (unsigned char)free[f];
			else
				dest.free[f] = 0;
		}

		int ofs = 0;

		for (int col = 0 ; col < 8 ; col++)
		{
			for (int k = 0 ; k < num_rows[col] ; k++)
			{
				dest.board[ofs++] = (unsigned char)column[col][k];
			}

			dest.board[ofs++] = COLUMN_END;
		}

		for ( ; ofs < 64 ; ofs++)
			dest.board[ofs] = 0;
	}

public:
	void add_card_to_column(int col, Card_t card)
	{
		column[col][num_rows[col]] = card;

		num_rows[col] += 1;
	}

	void remove_card_from_column(int col)
	{
		if (num_rows[col] <= 0)
			FatalError("remove_card_from_column. empty!\n");

		num_rows[col] -= 1;

		column[col][num_rows[col]] = (Card_t) 0;
	}

	void add_card_to_free(Card_t card)
	{
		if (num_free >= MAX_FREE)
			FatalError("add_card_to_free. full!\n");

		free[num_free] = card;

		num_free += 1;

		// sort the cards in the free cells (less states to try)
		// [ FIXME : this only handles MAX_FREE == 2 ]
		if (num_free > 1)
		{
			if (free[0] < free[1])
			{
				std::swap(free[0], free[1]);
			}
		}
	}

	void remove_card_from_free(int f)
	{
		if (num_free <= 0 || free[f] == 0)
			FatalError("remove_card_from_free: unset!\n");

		for (int i = f ; i + 1 < MAX_FREE ; i++)
			free[i] = free[i + 1];

		num_free -= 1;

		free[num_free] = 0;
	}

	bool check_victory() const
	{
		for (int suit = 0 ; suit < 4 ; suit++)
		{
			if (pile[suit] < 13)
				return false;
		}

		return true;
	}

	void move_card_to_pile(Card_t card)
	{
		int    rank = card_rank(card);
		Suit_e suit = card_suit(card);

		pile[suit] = rank;
	}

	bool can_move_card_to_pile(Card_t card) const
	{
		int    rank = card_rank(card);
		Suit_e suit = card_suit(card);

		return (rank == pile[suit] + 1);
	}

	bool can_move_card_to_column(Card_t card, int col, int from) const
	{
		// check if destination column is empty
		int size = num_rows[col];

		if (size > 0)
		{
			return can_stack_card(card, column[col][size - 1]);
		}

		// can move any card to an empty column
		// [ except if that card is the sole one in its column ]
		// [ also if there is a choice of column, then always pick left most ]

		if (from >= 0 && num_rows[from] == 1)
		  return false;

		int num_empties = 0;
		int first_empty = -1;

		for (int i = 0 ; i < 8 ; i++)
		{
			if (num_rows[i] == 0)
			{
				num_empties = num_empties + 1;
				first_empty = i;
			}
		}

		if (num_empties >= 2 && col != first_empty)
		  return false;

		// OK
		return true;
	}

	void dump_it() const
	{
		printf("%d/%d/%d/%d:", pile[0], pile[1], pile[2], pile[3]);

		for (int f = 0 ; f < num_free ; f++)
		{
			printf("%s", card_to_string(free[f]));
		}

		printf(":");

		for (int col = 0 ; col < 8 ; col++)
		{
			for (int i = 0 ; i < num_rows[col] ; i++)
			{
				printf("%s", card_to_string(column[col][i]));
			}

			if (col < 7)
				printf("/");
		}

		printf("\n");
	}

	void swap_columns(int a, int b)
	{
		std::swap(num_rows[a], num_rows[b]);

		for (int k = 0 ; k < MAX_COLUMN ; k++)
		{
			std::swap(column[a][k], column[b][k]);
		}
	}

	int score_column(int col)
	{
		int value = num_rows[col] * 10000;

		if (num_rows[col] > 0)
		{
			value += column[col][0] * 10;
		}

		return value + col;  // tie breaker
	}

	int find_best_column(int s, int e)
	{
		int best = s;
		int best_score = score_column(s);

		for (s++ ; s <= e ; s++)
		{
			int score = score_column(s);

			if (score > best_score)
			{
				best = s;
				best_score = score;
			}
		}

		return best;
	}

	void sort_columns()
	{
		for (int col = 0 ; col < 7 ; col++)
		{
			int best = find_best_column(col, 7);

			if (best != col)
				swap_columns(best, col);
		}
	}
};


struct Pending_state_t
{
	Packed_state_t  pack;

	// move used to get here (summary)
	Card_t card;

	char dest;
	unsigned char d_col;

	Pending_state_t *parent;

	// next in list
	Pending_state_t *next;
};


// various global state

int SEEN_CARDS[64];


typedef std::map<Packed_state_t, unsigned char>  Packed_map_t;

Packed_map_t  VISITED_STATES;

int NUM_VISITED_STATES = 0;


Pending_state_t * SOLVE_LIST;

Pending_state_t * CUR_NODE;


int symbol_to_rank(char sym)
{
	switch (tolower(sym))
	{
		case 'a': return 1;
		case '1': return 1;
		case '2': return 2;
		case '3': return 3;
		case '4': return 4;
		case '5': return 5;
		case '6': return 6;
		case '7': return 7;
		case '8': return 8;
		case '9': return 9;
		case '0': return 10;
		case 't': return 10;
		case 'j': return 11;
		case 'q': return 12;
		case 'k': return 13;
	}

	FatalError("bad rank symbol: %c\n", sym);
	return 0;
}


const char RANK_TO_SYMBOL[14] =
{
	'x',  // zero is not a valid rank

	'A', '2', '3', '4', '5', '6', '7',
	'8', '9', 'T', 'J', 'Q', 'K'
};


Suit_e symbol_to_suit(char sym)
{
	switch (tolower(sym))
	{
		case 'c': return SUIT_Clubs;
		case 'd': return SUIT_Diamonds;
		case 's': return SUIT_Spades;
		case 'h': return SUIT_Hearts;
	}

	FatalError("bad suit symbol: %c\n", sym);
	return SUIT_Clubs;
}


const char SUIT_TO_SYMBOL[4] = { 'c', 'd', 's', 'h' };


static const char * card_to_string(const Card_t card)
{
	static char buffer[20];

	buffer[0] = RANK_TO_SYMBOL[card_rank(card)];
	buffer[1] = SUIT_TO_SYMBOL[card_suit(card)];
	buffer[2] = 0;

	return buffer;
}


Card_t parse_next_card(char *line)
{
	// NOTE: this replaces the card characters with blanks
	//       [ so that each subsequent call reads the next token ]

	while (isspace(*line))
		line++;

	if (*line == 0)
		return 0;

	char rank = *line;  *line++ = ' ';

	if (*line == 0)
	{
		FatalError("malformed card on line.\n");
	}

	char suit = *line;  *line++ = ' ';

	if (*line != 0 && ! isspace(*line))
	{
		FatalError("malformed card on line.\n");
	}

	// handle the "xx" place-holders
	if (rank == 'x' && suit == 'x')
		return 0;

	Card_t card = card_create(symbol_to_rank(rank), symbol_to_suit(suit));

	if (SEEN_CARDS[card])
	{
		FatalError("duplicate card: %c%c\n", rank, suit);
	}

	SEEN_CARDS[card] = 1;

	return card;
}


void parse_row(Game_state_c& S, char *line)
{
	Card_t C1, C2, C3, C4;
	Card_t C5, C6, C7, C8;

	C1 = parse_next_card(line);
	C2 = parse_next_card(line);
	C3 = parse_next_card(line);
	C4 = parse_next_card(line);

	C5 = parse_next_card(line);
	C6 = parse_next_card(line);
	C7 = parse_next_card(line);
	C8 = parse_next_card(line);

	if (! C1 || ! C2 || ! C3 || ! C4 ||
	    (C5 && (! C6 || ! C7 || ! C8)))
	{
		FatalError("bad line in board file.\n");
	}

	if (C1) S.add_card_to_column(0, C1);
	if (C2) S.add_card_to_column(1, C2);
	if (C3) S.add_card_to_column(2, C3);
	if (C4) S.add_card_to_column(3, C4);

	if (C5) S.add_card_to_column(4, C5);
	if (C6) S.add_card_to_column(5, C6);
	if (C7) S.add_card_to_column(6, C7);
	if (C8) S.add_card_to_column(7, C8);
}


void load_cards(Game_state_c& S, const char *filename)
{
	FILE *fp = fopen(filename, "r");

	if (! fp)
	{
		FatalError("Cannot open file: %s\n%s\n\n", filename, strerror(errno));
	}

	static char line_buf[MSG_BUF_LEN];

	while (fgets(line_buf, MSG_BUF_LEN, fp))
	{
		parse_row(S, line_buf);
	}

	fclose(fp);
}


void check_all_cards_used(Suit_e suit)
{
	for (int rank = 1 ; rank <= 13 ; rank++ )
	{
		Card_t card = card_create(rank, suit);

		if (! SEEN_CARDS[card])
		{
			FatalError("missing card: %c%c\n", RANK_TO_SYMBOL[rank], SUIT_TO_SYMBOL[suit]);
		}
	}
}


void add_new_state(Game_state_c& S, Card_t card = 0, const char *dest = NULL, int d_col = 0)
{
	Pending_state_t *node = new Pending_state_t;

	S.sort_columns();

	S.pack_it(node->pack);


	// ignore states which we have seen before
	Packed_map_t::iterator pos = VISITED_STATES.find(node->pack);

	if (pos != VISITED_STATES.end())
	{
		delete node;

		return;
	}


	node->card  = card;
	node->dest  = dest ? dest[0] : 0;
	node->d_col = d_col;  // FIXME : wrong after sorting columns

	node->parent = CUR_NODE;

	// add to head of list
	node->next = SOLVE_LIST;

	SOLVE_LIST = node;
}


void promote_old_state()
{
	Pending_state_t *node = SOLVE_LIST;

	if (! node) return;
	if (! node->next) return;
	if (! node->next->next) return;
	if (! node->next->next->next) return;

	while (node->next && node->next->next)
		node = node->next;

	Pending_state_t *last = node->next;

	node->next = NULL;

	// add to head of list
	last->next = SOLVE_LIST;

	SOLVE_LIST = last;
}


void apply_a_move(const Game_state_c& S, const char *source, int s_col, const char *dest, int d_col, Card_t card)
{
/// DEBUG
/// fprintf(stderr, "apply_a_move: %s --> %s\n", source, dest);

	// copy the existing game-state
	Game_state_c T(S);

	// remove card from existing spot
	if (source[0] == 'f')
	{
		T.remove_card_from_free(s_col);
	}
	else if (source[0] == 'c')
	{
		T.remove_card_from_column(s_col);
	}
	else
	{
		FatalError("bad source in move\n");
	}

	// insert card at new spot
	if (dest[0] == 'p')
	{
		T.move_card_to_pile(card);
	}
	else if (dest[0] == 'f')
	{
		T.add_card_to_free(card);
	}
	else if (dest[0] == 'c')
	{
		T.add_card_to_column(d_col, card);
	}
	else
	{
		FatalError("bad dest in move\n");
	}

	// finally, add it to list of states to try
	add_new_state(T, card, dest, d_col);
}


void detect_possible_moves(const Game_state_c& S)
{
	int col, d, f;

	Card_t card;

	// movement from a COLUMN to the FREE area

	for (col = 0 ; col < 8 ; col++)
	{
		if (S.num_rows[col] == 0)
			continue;

		if (S.num_free < MAX_FREE)
		{
			card = S.column[col][S.num_rows[col] - 1];

			apply_a_move(S, "column", col, "free", -1, card);
		}
	}

	// movement from the FREE area to a column

	for (f = 0 ; f < S.num_free ; f++)
	{
		card = S.free[f];

		for (d = 0 ; d < 8 ; d++)
		{
			if (S.can_move_card_to_column(card, d, -1 /* FREE */))
			{
				apply_a_move(S, "free", f, "column", d, card);
			}
		}
	}

	// movement from one COLUMN to another COLUMN

	for (col = 0 ; col < 8 ; col++)
	{
		if (S.num_rows[col] == 0)
			continue;

		card = S.column[col][S.num_rows[col] - 1];

		for (d = 0 ; d < 8 ; d++)
		{
			if (S.can_move_card_to_column(card, d, col))
			{
				apply_a_move(S, "column", col, "column", d, card);
			}
		}
	}

	// movements from a COLUMN to the PILE

	for (col = 0 ; col < 8 ; col++)
	{
		if (S.num_rows[col] == 0)
			continue;

		card = S.column[col][S.num_rows[col] - 1];

		if (S.can_move_card_to_pile(card))
		{
			apply_a_move(S, "column", col, "pile", -1, card);
		}
	}

	// movement from FREE area to the PILE

	for (f = 0 ; f < S.num_free ; f++)
	{
		card = S.free[f];

		if (S.can_move_card_to_pile(card))
		{
			apply_a_move(S, "free", f, "pile", -1, card);
		}
	}
}


/*
function move_to_string(move)
  local str = card_to_string(move.card)

  str = str .. " //> "

  if move.dest == "pile" then
    str = str .. "PILE"
  elseif move.dest == "free" then
    str = str .. "free"
  else
    str = str .. string.format("col %d", move.dest_col)
  end

  return str
end
*/


/*
void update_best_seen(S, str)
  local A = 99
  local B = 0
  local C = 0

  for _,val in pairs(S.pile) do
    A = math.min(A, val)
    B = math.max(B, val)
    C = C + val
  end

  local free_cols = 0

  for i = 1, 8 do
    if #S.column[i] == 0 then
      free_cols = free_cols + 1
    end
  end

  local score = A * 1000 + B * 100 + C * 10 + free_cols

  if BEST_SEEN_STATE == "" or score > BEST_SEEN_STATE.score then
    BEST_SEEN_STATE = S
    S.score = score
print("new best state:", str)
  end
}
*/


/*
inline bool check_victory_raw(Packed_state_t& P)
{
	return	(P.pile1 == (13 * 16 + 13)) &&
			(P.pile2 == (13 * 16 + 13));
}
*/

void show_solution_recurse(Pending_state_t *node)
{
	if (node->parent)
		show_solution_recurse(node->parent);

	if (node->card)
	{
		fprintf(stderr, "  %s --> ", card_to_string(node->card));

		if (node->dest == 'p')
			fprintf(stderr, "PILE\n");
		else if (node->dest == 'f')
			fprintf(stderr, "free\n");
		else
			fprintf(stderr, "col %d\n", node->d_col + 1);
	}
}


void show_solution(Pending_state_t *node)
{
	fprintf(stderr, "\n");
	fprintf(stderr, "SOLUTION :\n");
	fprintf(stderr, "\n");

	show_solution_recurse(node);
}


bool try_solve(Packed_state_t& P)
{
	// ignore states which we have seen before
	Packed_map_t::iterator pos = VISITED_STATES.find(P);

	if (pos != VISITED_STATES.end())
		return false;

	VISITED_STATES[P] = 1;

	NUM_VISITED_STATES += 1;

	Game_state_c S(P);

	if ((NUM_VISITED_STATES % 1000) == 0)
	{
		printf("trying state: %d  [%d %d %d %d]\n", NUM_VISITED_STATES,
				S.pile[0], S.pile[1], S.pile[2], S.pile[3]);
	}

//FIXME  update_best_seen(S, str)

	// succeed if this state has all cards on the piles
	if (S.check_victory())
	{
//!!!		show_solution(CUR_NODE);

		return true;
	}

	// add all possible moves to the list to try
	detect_possible_moves(S);

//??	if ((NUM_VISITED_STATES % 1000) == 1)
//??		promote_old_state();

	return false;
}


bool solve()
{
	while (SOLVE_LIST)
	{
		// remove the head of the list
		Pending_state_t *node = SOLVE_LIST;

		SOLVE_LIST = node->next;

		CUR_NODE   = node;

		if (try_solve(node->pack))
		{
			return true;
		}

		delete node;
	}

	return false;
}


int main(int argc, char **argv)
{
	Game_state_c ROOT;

	if (argc < 2)
	{
		FatalError("Missing filename for board.\n");
	}

	load_cards(ROOT, argv[1]);

	for (int suit = 0 ; suit < 4 ; suit++)
	{
		check_all_cards_used((Suit_e) suit);
	}

	add_new_state(ROOT);

	bool result = solve();

	printf("\n");
	printf("Looked at %d game-states\n", NUM_VISITED_STATES);
	printf("\n");

	if (result)
		printf("SOLVED !!!!\n");
	else
		printf("No solution found\n");

	return 0;
}


//--- editor settings ---
// vi:ts=4:sw=4:noexpandtab
