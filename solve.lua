--
--  Two-Cell Solver
--
--  by Andrew Apted, 2017.
--
--  License of this code is CC0 (public domain).
--  USE AT OWN RISK.
--
--  NOTE: this Lua version much too slow and uses too much memory
--        to solve many boards -- use the C++ version instead.
--


SEEN_CARDS = {}

VISITED_STATES = {}
NUM_VISITED_STATES = 0
BEST_SEEN_STATE = ""

MAX_FREE = 2

SOLVE_LIST = {}


SYMBOL_TO_RANK =
{
  ['a'] = 1,
  ['1'] = 1,
  ['2'] = 2,
  ['3'] = 3,
  ['4'] = 4,
  ['5'] = 5,
  ['6'] = 6,
  ['7'] = 7,
  ['8'] = 8,
  ['9'] = 9,
  ['0'] = 10,
  ['t'] = 10,
  ['j'] = 11,
  ['q'] = 12,
  ['k'] = 13,
}

RANK_TO_SYMBOL =
{
  'A',
  '2',
  '3',
  '4',
  '5',
  '6',
  '7',
  '8',
  '9',
  'T',
  'J',
  'Q',
  'K',
}


ALL_SUITS =
{
  "clubs",
  "diamonds",
  "spades",
  "hearts"
}

SYMBOL_TO_SUIT =
{
  ['c'] = "clubs",
  ['d'] = "diamonds",
  ['s'] = "spades",
  ['h'] = "hearts",
}

SUIT_TO_SYMBOL =
{
  ["clubs"]    = 'c',
  ["diamonds"] = 'd',
  ["spades"]   = 's',
  ["hearts"]   = 'h',
}

SUIT_SCORES =
{
  ["clubs"]    = 1,
  ["diamonds"] = 2,
  ["spades"]   = 3,
  ["hearts"]   = 4,
}

SUIT_COLORS =
{
  ["clubs"]    = "black",
  ["diamonds"] = "red",
  ["spades"]   = "black",
  ["hearts"]   = "red",
}


function new_state()
  local S =
  {
    pile   = {},
    free   = {},
    column = {}
  }

  S.pile["clubs"]    = 0
  S.pile["diamonds"] = 0
  S.pile["spades"]   = 0
  S.pile["hearts"]   = 0

  for i = 1, 8 do
    S.column[i] = {}
  end

  return S
end


function copy_state(S)
  local T = new_state()

  T.pile["clubs"]    = S.pile["clubs"]
  T.pile["diamonds"] = S.pile["diamonds"]
  T.pile["spades"]   = S.pile["spades"]
  T.pile["hearts"]   = S.pile["hearts"]

  for _,card in ipairs(S.free) do
    table.insert(T.free, card)
  end

  for i = 1, 8 do
    for _,card in ipairs(S.column[i]) do
      table.insert(T.column[i], card)
    end
  end

  return T
end


function card_to_string(CARD)
  local A = assert(RANK_TO_SYMBOL[CARD.rank])
  local B = assert(SUIT_TO_SYMBOL[CARD.suit])

  return A .. B
end


function state_to_string(S)
  local str

  str =               S.pile["clubs"]
  str = str .. "/" .. S.pile["diamonds"]
  str = str .. "/" .. S.pile["spades"]
  str = str .. "/" .. S.pile["hearts"]

  str = str .. ":"

  for _,card in ipairs(S.free) do
    str = str .. card_to_string(card)
  end

  str = str .. ":"

  for i = 1, 8 do
    for _,card in ipairs(S.column[i]) do
      str = str .. card_to_string(card)
    end

    if i < 8 then
      str = str .. "/"
    end
  end

  return str
end


function parse_card(str)
  if string.len(str) ~= 2 then
    error("bad card: " .. str)
  end

  str = string.lower(str)

  local rank, suit = string.match(str, "(%w)(%w)")

  if rank == nil or suit == nil then
    error("strange card: " .. str)
  end

  local CARD = {}

  CARD.rank = SYMBOL_TO_RANK[rank]
  CARD.suit = SYMBOL_TO_SUIT[suit]

  if CARD.rank == nil then
    error("unknown rank in card: " .. str)
  end

  if CARD.suit == nil then
    error("unknown suit in card: " .. str)
  end

  local name = CARD.suit .. CARD.rank

  if SEEN_CARDS[name] then
    error("duplicate card: " .. str)
  end

  SEEN_CARDS[name] = true

  -- compute a score for each card (1..99)
  CARD.score = CARD.rank * 4 + SUIT_SCORES[CARD.suit]
  CARD.inv_score = 99 - CARD.score

  return CARD
end


function parse_row(S, line)
  local C1, C2, C3, C4, C5, C6, C7, C8 =
      string.match(line, "^(%w%w) (%w%w) (%w%w) (%w%w) (%w%w) (%w%w) (%w%w) (%w%w)")

  if C1 == nil or C2 == nil or C3 == nil or C4 == nil or
     C5 == nil or C6 == nil or C7 == nil or C8 == nil
  then
    error("bad line: " .. tostring(line))
  end

  if C1 ~= "xx" then table.insert(S.column[1], parse_card(C1)) end
  if C2 ~= "xx" then table.insert(S.column[2], parse_card(C2)) end
  if C3 ~= "xx" then table.insert(S.column[3], parse_card(C3)) end
  if C4 ~= "xx" then table.insert(S.column[4], parse_card(C4)) end

  if C5 ~= "xx" then table.insert(S.column[5], parse_card(C5)) end
  if C6 ~= "xx" then table.insert(S.column[6], parse_card(C6)) end
  if C7 ~= "xx" then table.insert(S.column[7], parse_card(C7)) end
  if C8 ~= "xx" then table.insert(S.column[8], parse_card(C8)) end
end


function load_cards(filename)
  SEEN_CARDS = {}

  local fp, err = io.open(filename, "r")

  if not fp then
    error("Cannot open file: " .. tostring(err))
  end

  local S = new_state()

  for line in fp:lines() do
    parse_row(S, line)
  end

  fp:close()

  return S
end


function check_all_cards_used(suit)
  for i = 1, 13 do
    local name = suit .. i

    if not SEEN_CARDS[name] then
      error("missing card: " .. name)
    end
  end
end


function can_stack_card(card, onto)
  if card.rank + 1 ~= onto.rank then return false end

  local A = SUIT_COLORS[card.suit]
  local B = SUIT_COLORS[onto.suit]

  return A ~= B
end


function can_move_card_to_column(S, card, C, from_C)
  -- can move any card to an empty column
  -- [ except if that card is the sole one in its column ]
  -- [ also if there is a choice of column, then always pick left most ]
  if #C == 0 then
    if from_C ~= "free" and #from_C == 1 then
      return false
    end

    local num_empties = 0
    local first_empty

    for i = 1, 8 do
      if #S.column[i] == 0 then
        num_empties = num_empties + 1
        first_empty = S.column[i]
      end
    end

    if num_empties > 2 and C ~= first_empty then
      return false
    end

    return true
  end

  -- otherwise must be able to stack it on last card in column
  return can_stack_card(card, C[#C])
end


function collect_possible_moves(S)
  local moves = {}

  -- movements from the FREE area --

  for col,card in ipairs(S.free) do
    if card.rank == S.pile[card.suit] + 1 then
      local MOVE =
      {
        card = card,
        dest = "pile",
        source = "free",
        source_col = col,
        score = 900 + card.inv_score
      }
      table.insert(moves, MOVE)
    end

    for i = 1, 8 do
      if can_move_card_to_column(S, card, S.column[i], "free") then
        local MOVE =
        {
          card = card,
          dest = "column",
          dest_col = i,
          source = "free",
          source_col = col,
          score = 400 + card.score
        }
        table.insert(moves, MOVE)
      end
    end
  end

  -- movements from the COLUMNS --

  for col = 1, 8 do
    local C = S.column[col]

    if #C > 0 then
      local card = C[#C]

      if card.rank == S.pile[card.suit] + 1 then
        local MOVE =
        {
          card = card,
          dest = "pile",
          source = "column",
          source_col = col,
          score = 800 + card.inv_score
        }
        table.insert(moves, MOVE)
      end

      if #S.free < MAX_FREE then
        local MOVE =
        {
          card = card,
          dest = "free",
          source = "column",
          source_col = col,
          score = 100 + card.score
        }
        table.insert(moves, MOVE)
      end

      for i = 1, 8 do
        if can_move_card_to_column(S, card, S.column[i], S.column[col]) then
          local MOVE =
          {
            card = card,
            dest = "column",
            dest_col = i,
            source = "column",
            source_col = col,
            score = 700 + card.score
          }
          table.insert(moves, MOVE)
        end
      end
    end
  end

  -- sort moves by their "chance of winning" score
  -- [ best is LAST since we grab states from end of SOLVE_LIST ]
  table.sort(moves, function(A, B) return A.score < B.score end)

  return moves
end


function apply_move(S, move)
  local T = copy_state(S)

  T.move = move
  T.parent = S

  -- remove card from existing spot
  if move.source == "free" then
    assert(#T.free >= move.source_col)

    table.remove(T.free, move.source_col)

  elseif move.source == "column" then
    local C = T.column[move.source_col]
    assert(#C > 0)

    table.remove(C, #C)
  else
    error("bad source in move")
  end

  -- insert card at new spot
  if move.dest == "pile" then
    T.pile[move.card.suit] = move.card.rank

  elseif move.dest == "free" then
    table.insert(T.free, move.card)

    -- sort the cards in the free cells (less states to try)
    if #T.free >= 2 then
      table.sort(T.free, function(A, B) return A.score > B.score end)
    end

  elseif move.dest == "column" then
    table.insert(T.column[move.dest_col], move.card)

  else
    error("bad dest in move")
  end

  return T
end


function move_to_string(move)
  local str = card_to_string(move.card)

  str = str .. " --> "

  if move.dest == "pile" then
    str = str .. "PILE"
  elseif move.dest == "free" then
    str = str .. "free"
  else
    str = str .. string.format("col %d", move.dest_col)
  end

  return str
end


function check_victory(S)
  for _,suit in ipairs(ALL_SUITS) do
    if S.pile[suit] < 13 then
      return false
    end
  end

  return true
end


function update_best_seen(S, str)
  local A = 99
  local B = 0
  local C = 0

  for _,val in pairs(S.pile) do
    A = math.min(A, val)
    B = math.max(B, val)
    C = C + val
  end

  local free_cols = 0

  for i = 1, 8 do
    if #S.column[i] == 0 then
      free_cols = free_cols + 1
    end
  end

  local score = A * 1000 + B * 100 + C * 10 + free_cols

  if BEST_SEEN_STATE == "" or score > BEST_SEEN_STATE.score then
    BEST_SEEN_STATE = S
    S.score = score
print("new best state:", str)
  end
end


function try_solve(S)
  local str = state_to_string(S)

  -- ignore states which we have seen before
  if VISITED_STATES[str] then
    return false
  end

  VISITED_STATES[str] = true
  NUM_VISITED_STATES = NUM_VISITED_STATES + 1

  if (NUM_VISITED_STATES % 10000) == 1 then
    print("trying state ", NUM_VISITED_STATES)
  end

  update_best_seen(S, str)

  -- succeed if this state has all cards on the piles
  if check_victory(S) then
    return true
  end

  -- add all possible moves to the list to try
  for _,move in ipairs(collect_possible_moves(S)) do
    local T = apply_move(S, move)
    table.insert(SOLVE_LIST, T)
  end

  return false
end


function solve()
  while true do
    local len = #SOLVE_LIST

    if len == 0 then
      break;
    end

    local S = table.remove(SOLVE_LIST, len)

    if try_solve(S) then
      return true
    end
  end

  return false
end


function main()
  if arg[1] == nil then
    error("Missing filename of cards.")
  end

  local ROOT = load_cards(arg[1])

  for _,suit in ipairs(ALL_SUITS) do
    check_all_cards_used(suit)
  end

  SOLVE_LIST = { ROOT }

  local result = solve()

  print()
  print(string.format("Looked at %d game-states", NUM_VISITED_STATES))
  print()

  if result then
    print("SOLVED !!!!")
  else
    print("No solution found")
  end
end


-- go baby go!
main()

